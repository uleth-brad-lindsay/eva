package ca.uleth.geneticalgorithm;

import java.util.ArrayList;
import java.util.Random;
import ca.uleth.classification.*;

public class Chromosome implements Comparable<Chromosome> {

    /**
     * This sets the gene length of the chromosome.
     */
    private static int geneLength = 50;

    /**
     * This sets the ideal number of selected features for our genetic algorithm.
     */
    private static int idealSelectedFeatures = 0;

    /**
     * Double representation of chromosomal fitness. This is based on feedback from the
     * k-nn classifier given selected features.
     */
    private double fitness;

    /**
     * This is our internal storage for the genetic components of our chromosome.
     */
    private boolean[] genes;

    /**
     * This is an integer array of genes to skip for classification.
     */
    private int[] genesToSkip;

    /**
     * Flag indicating whether or not we should calculate fitness when
     * get fitness it called.
     */
    private boolean calcFitness;

    /**
     * This is a count of the genes set for this chromosome.
     */
    private int genesSet;

    /**
     * Initializer for our chromosome.
     */
    public Chromosome() {
        this.fitness = 0.0;
        this.calcFitness = true;
        this.genes = new boolean[geneLength];
        this.genesToSkip = new int[0];
    }

    /**
     * Create a chromosome with a random seeded number of genes.
     */
    public void createChromosome() {
        Random rand = new Random(System.nanoTime());

        for (int i = 0; i < genes.length; i++) {
            // dividing out ideal selected attributes by our full gene length.
            // and comparing to a random float for random setting of genes.
            boolean set = (rand.nextFloat() < (float) idealSelectedFeatures / (float) geneLength);
            this.genes[i] = set;
        }
    }

    /**
     * Sets the gene length for all chromosomes.
     *
     * @param value gene length.
     */
    public static void setGeneLength(int value) {
        geneLength = value;
    }

    /**
     * Sets the ideal number of selected features for the chromosome.
     *
     * @param value the ideal number of selected features.
     */
    public static void setIdealSelectedFeatures(int value) {
        idealSelectedFeatures = value;
    }

    /**
     * Gets a gene at a selected index.
     *
     * @param index index to get gene from.
     * @return returns a boolean representation of a gene state.
     */
    public boolean getGene(int index) {
        return genes[index];
    }

    /**
     * Sets a gene at a selected index.
     *
     * @param index index of the gene to set.
     * @param value the value to set the gene to.
     */
    public void setGene(int index, boolean value) {
        genes[index] = value;
        fitness = 0.0;
    }

    /**
     * Gets the total length of the chromosome.
     *
     * @return integer representing the length.
     */
    public int size() {
        return genes.length;
    }

    /**
     * Gets the number of genes set for this chromosome.
     *
     * @return integer representing the number of genes set
     * for this chromosome.
     */
    public int getGenesSet() {
        genesSet = 0;
        for (int i = 0; i < size(); i++) {
            if (genes[i]) genesSet++;
        }
        return genesSet;
    }

    /**
     * Gets a listing of genes to skip from this chromosome.
     *
     * @return an integer array containing the indices of the attributes to skip.
     */
    public int[] getGenesToSkip() {
        if (genesToSkip == null || genesToSkip.length == 0)
        {
            ArrayList<Integer> skips = new ArrayList<>();
            for (int i = 0; i < size(); i++) {
                if (i != Knn.getClassIndex()) {
                    if (!genes[i])skips.add(i);
                }
            }

            int[] result = new int[skips.size()];
            for (int i = 0; i < skips.size(); i++) {
                result[i] = skips.get(i);
            }
            genesToSkip = result;
        }

        return genesToSkip;
    }

    /**
     * Recalculates the fitness if needed and returns a fitness value.
     * Recalculation will occur if the recalculate flag is set.
     *
     * @return double value representing fitness.
     */
    public double getFitness() {
        if (calcFitness) {
            fitness = Fitness.getFitness(this);
            calcFitness = false;
        }
        return fitness;
    }

    /**
     * Returns a human readable representation of the chromosome.
     *
     * @return String representation of the chromosome.
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size(); i++) {
            sb.append(getGene(i) ? "1" : "0");
        }
        return sb.toString();
    }

    /**
     * Basic comparator function based on fitness level.
     *
     * @param o chromosome we're comparing ourselves to.
     * @return 1 if we're more fit, -1 if we're less fit, 0 if we're equally fit.
     */
    @Override
    public int compareTo(Chromosome o) {
        if (fitness < o.getFitness()) {
            return -1;
        } else if (fitness > o.getFitness()) {
            return 1;
        }
        return 0;
    }
}
