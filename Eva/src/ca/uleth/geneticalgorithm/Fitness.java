package ca.uleth.geneticalgorithm;

import ca.uleth.classification.Knn;

public class Fitness {

    public static double idealFitness() {
        // Weka was used to determine this value.
        // using SSFS we get a 79.76% classification accuracy using 23 attributes.
        // we're yielding 3.46% accuracy per attribute, we should aim for at least that much.
        return 3.46;
    }

    public static double getFitness(Chromosome c) {
        try {
            Knn.run(c.getGenesToSkip());
//            double fitness = (0.5 * c.getGenesToSkip().length) +
//                    Knn.getPctCorrect() +
//                    Knn.getSensitivity() +
//                    Knn.getSpecificity();
//            return fitness;

            // get percentage per gene set;
            return Knn.getPctCorrect() / c.getGenesSet();
        } catch (Exception e) {
            return 0;
        }
    }
}
