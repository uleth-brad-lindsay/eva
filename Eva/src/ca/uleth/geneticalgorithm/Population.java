package ca.uleth.geneticalgorithm;

public class Population {

    private Chromosome[] chromosomes;

    public Population(int size, boolean initialize) {
        chromosomes = new Chromosome[size];

        if (initialize) {
            for (int i = 0; i < size(); i++) {
                Chromosome c = new Chromosome();
                c.createChromosome();
                save(i, c);
            }
        }
    }

    /**
     * Gets the size of the population.
     *
     * @return
     */
    public int size() {
        return chromosomes.length;
    }

    /**
     * Gets a chromosome at a defined index.
     *
     * @param index index of the chromosome.
     * @return returns a chromosome.
     */
    public Chromosome getChromosome(int index) {
        return chromosomes[index];
    }

    /**
     * Gets the most fit chromosome.
     *
     * @return Chromosome that is the most elite.
     */
    public Chromosome getMostFit() {
        return getMostLeast(true);
    }

    /**
     * Gets the least fit chromosome.
     *
     * @return Chromosome that is the least elite.
     */
    public Chromosome getLeastFit() {
        return getMostLeast(false);
    }

    /**
     * Gets either the most or least fit chromosome based on parameters.
     *
     * @param most if true, get most fit, else get least fit.
     * @return Chromosome that is either most or least fit based on parameters.
     */
    private Chromosome getMostLeast(boolean most) {
        Chromosome selected = null;
        double fitness = most ? Double.MIN_VALUE : Double.MAX_VALUE;

        for (int i = 0; i < size(); i++) {
            boolean mostCheck = most && chromosomes[i].getFitness() > fitness;
            boolean leastCheck = !most && chromosomes[i].getFitness() < fitness;

            if (mostCheck || leastCheck) {
                selected = chromosomes[i];
                fitness = selected.getFitness();
            }
        }

        return selected;
    }

    /**
     * Gets the aggregate fitness for this population.
     *
     * @return double value representing the accumulated fitness of
     * this population.
     */
    public double getFitness() {
        double totalFitness = 0.0;
        for (int i = 0; i < size(); i++) {
            totalFitness += chromosomes[i].getFitness();
        }
        return totalFitness;
    }

    /**
     * Saves a chromosome at a defined index.
     *
     * @param index      index to save the chromosome.
     * @param chromosome chromosome to save.
     */
    public void save(int index, Chromosome chromosome) {
        chromosomes[index] = chromosome;
    }
}
