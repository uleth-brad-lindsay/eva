package ca.uleth.geneticalgorithm;

import java.util.Random;

public class GeneticAlgorithm {

    /**
     * This is the probability that we will cross over
     **/
    private static double crossoverProbability = 0.3;

    /**
     * This is the given mutation rate for minimal changes to our child chromosome
     **/
    private static double mutationRate = 0.01;

    /**
     * This is the header method for this entire algorithm, it will evolve a population
     * and return a derived population based on selection, crossover, and mutation.
     *
     * @param population This is the population we're using as our basis. We derive from this population.
     * @return A population that is made up of mutate
     */
    public static Population evolve(Population population) {
        Population nextPopulation = new Population(population.size(), false);

        for (int i = 0; i < population.size(); i++) {
            Chromosome c1 = tournamentSelect(population);
            Chromosome c2 = tournamentSelect(population);
//            Chromosome c1 = rouletteSelect(population, true);
//            Chromosome c2 = rouletteSelect(population, false);
            Chromosome nextGen = crossover(c1, c2);
            nextPopulation.save(i, nextGen);
        }

        for (int i = 0; i < nextPopulation.size(); i++) {
            mutate(nextPopulation.getChromosome(i));
        }

        return nextPopulation;
    }

    /**
     * This algorithm is used to generate a child from two parents.
     *
     * @param c1 Parental Chromosome 1.
     * @param c2 Parental Chromosome 2.
     * @return child that is a combination of both chromosome 1 and chromosome 2.
     */
    public static Chromosome crossover(Chromosome c1, Chromosome c2) {
        Random rand = new Random();
        Chromosome child = new Chromosome();

        for (int i = 0; i < c1.size(); i++) {
            if (rand.nextFloat() <= crossoverProbability) {
                child.setGene(i, c1.getGene(i));
            } else {
                child.setGene(i, c2.getGene(i));
            }
        }

        return child;
    }

    /**
     * Mutates the chromosome based on the given population mutation rate.
     *
     * @param chromosome the chromosome to mutate.
     */
    private static void mutate(Chromosome chromosome) {
        Random rand = new Random();
        // iterate through all of our genes.
        for (int i = 0; i < chromosome.size(); i++) {
            if (chromosome.getGene(i) && (rand.nextFloat() < mutationRate)) {
                // if the gene is on, and we underachieve on mutation rate, flip.
                chromosome.setGene(i, false);
            } else if ((!chromosome.getGene(i)) && (rand.nextFloat() < mutationRate)) {
                // if the gene is off, and we underachieve on mutation rate, flip.
                chromosome.setGene(i, true);
            }
        }
    }

    /**
     * This is a tournament rouletteSelect method, it selects random chromosomes from our
     * input population, then selects the most fit among them. This is a very simple
     * way of performing rouletteSelect.
     *
     * @param population Input population to select from.
     * @return Chromosome that is the most fit from the tournament.
     */
    private static Chromosome tournamentSelect(Population population) {
        Population tournament = new Population(7, false);
        Random r = new Random();

        for (int i = 0; i < tournament.size(); i++) {
            // we're selecting a random item out of our population.
            int randomId = (int) (r.nextFloat() * population.size());
            // then saving it to our tournament population.
            tournament.save(i, population.getChromosome(randomId));

        }

        // we return the most fit item of our tournament population.
        return tournament.getMostFit();
    }

    /**
     * This function selects a chromosome from the population based on the roulette wheel
     * method of extraction.
     *
     * @param population the population to select our candidate from.
     * @param maximizing whether or not we're looking for out maximum candidate.
     * @return Chromosome that is either a maximal or minimal candidate.
     */
    private static Chromosome rouletteSelect(Population population, boolean maximizing) {
        double sumFitness = population.getFitness();
        double maxFitness = population.getMostFit().getFitness();
        double minFitness = population.getLeastFit().getFitness();

        Random r = new Random();
        double p = r.nextFloat() * sumFitness;
        double t = maxFitness + minFitness;

        int first = 0;
        Chromosome chosen = population.getChromosome(first);

        for (int i = 0; i < population.size(); i++) {
            double fitness = population.getChromosome(i).getFitness();

            if (maximizing) {
                p -= population.getChromosome(i).getFitness();
            } else {
                p -= (t - fitness);
            }

            if (p < 0) {
                chosen = population.getChromosome(i);
                break;
            }
        }

        return chosen;
    }
}
