package ca.uleth;

import ca.uleth.classification.Knn;
import ca.uleth.geneticalgorithm.Chromosome;
import ca.uleth.geneticalgorithm.Fitness;
import ca.uleth.geneticalgorithm.GeneticAlgorithm;
import ca.uleth.geneticalgorithm.Population;

import java.io.File;

public class Main {

    /**
     * Application entry point.
     *
     * @param args args[0] filename to process.
     *             args[1] class index of file to process.
     *             args[2] neighbours to use for knn classification.
     *             args[3] number of folds to use during cross validation.
     */
    public static void main(String[] args) {
        welcome();
        try {
            if (!parseArgsCheck(args)) {
                help();
            } else {
                System.out.print("Initializing System...");
                Knn.initialize(args[0], Integer.parseInt(args[1]), Integer.parseInt(args[2]), Integer.parseInt(args[3]));
                //Knn.initialize("/Users/brad/data/ulc/training.csv", 0, 1, 10);
                Chromosome.setGeneLength(Knn.getAttributeCount());
                //Chromosome.setIdealSelectedFeatures(Knn.getAttributeCount() / 4);
                Chromosome.setIdealSelectedFeatures(Knn.getAttributeCount());
                System.out.println("Done");

                System.out.print("Creating Initial Population...");
                Population initialG = new Population(10, true);
                System.out.println("Done");

                System.out.println("Running...");
                int generation = 0;

                int[] lastBlock = new int[4];
                lastBlock[0] = 0;
                lastBlock[1] = 1;
                lastBlock[2] = 2;
                lastBlock[3] = 3;
                Chromosome mostFit;

                while (true) {
                    generation++;

                    System.out.println("Generation: " + generation);

                    mostFit = initialG.getMostFit();
                    System.out.println("Fittest: ");
                    System.out.println(String.format("%5s %-30s %-10f", "*", "Weighted Fitness", mostFit.getFitness()));
                    System.out.println(String.format("%5s %-30s %-10d", "*", "Features Selected", mostFit.getGenesSet()));
                    System.out.println(System.lineSeparator());

                    lastBlock[generation % 4] = (int) mostFit.getFitness();
                    if (lastBlock[0] == lastBlock[1] &&
                            lastBlock[1] == lastBlock[2] &&
                            lastBlock[2] == lastBlock[3] &&
                            mostFit.getFitness() >= Fitness.idealFitness()) {
                        break;
                    }

                    initialG = GeneticAlgorithm.evolve(initialG);
                }
                System.out.println("...DONE!");
                System.out.println(System.lineSeparator());

                // This is a total hack to get the correct classifier information.
                Knn.run(mostFit.getGenesToSkip());
                System.out.println(Knn.getMetrics());

            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Simple function for parsing arguments given to the application. Provides very basic checking.
     *
     * @param args Arguments passed in from the application entry point.
     * @return false if any arguments fail validation, true if everything passes checks.
     */
    private static boolean parseArgsCheck(String[] args) {
        if (args.length < 4) return false;
        if (args[0].length() == 0) return false;

        File f = new File(args[0]);
        if (!f.exists()) return false;

        try {
            if (Integer.parseInt(args[1]) < 0) return false;
            if (Integer.parseInt(args[2]) < 0) return false;
            if (Integer.parseInt(args[3]) < 0) return false;

        } catch (NumberFormatException n) {
            return false;
        }

        return true;
    }

    /**
     * Simple function to provide welcome text.
     */
    private static void welcome() {
        StringBuilder sb = new StringBuilder();
        sb.append("--------------------------------------------");
        sb.append(System.lineSeparator());
        sb.append("EVA version 0.01" + System.lineSeparator());
        sb.append("Author: Brad Lindsay 001199212" + System.lineSeparator());
        sb.append("--------------------------------------------");
        sb.append(System.lineSeparator());
        sb.append(System.lineSeparator());
        System.out.println(sb.toString());
    }

    /**
     * Simple function to provide application help in case of parameter validation failu
     */
    private static void help() {
        StringBuilder sb = new StringBuilder();
        String commandFormat = "%-30s %s" + System.lineSeparator();
        sb.append("EVA was improperly initialized." + System.lineSeparator());
        sb.append("Usage: java -jar eva <file> <class index> <neighbours> <folds>" + System.lineSeparator());
        sb.append(System.lineSeparator());
        sb.append(System.lineSeparator());
        sb.append(String.format(commandFormat, "file:", "file to process"));
        sb.append(System.lineSeparator());

        sb.append(String.format(commandFormat, "class index:", "integer between 0 - n where n"));
        sb.append(String.format(commandFormat, "", "is attributes in file"));
        sb.append(System.lineSeparator());

        sb.append(String.format(commandFormat, "neighbours:", "integer between 0 - n where n"));
        sb.append(String.format(commandFormat, "", "is the neighbours to use for the k-nn classifier"));
        sb.append(System.lineSeparator());

        sb.append(String.format(commandFormat, "folds:", "integer between 0 - n where n "));
        sb.append(String.format(commandFormat, "", "is the number of folds to use for cross validation"));

        System.out.println(sb.toString());
    }
}
